FROM maven:latest
COPY src /home/app/src
COPY pom.xml /home/app
WORKDIR /home/app
RUN mvn clean package -DskipTests
EXPOSE 8080
CMD mvn jetty:run