# KilledDB

**09/02/2022 -> 09/06/2022**

## Cahier des charges de la mission
- <a href="docs/specifications.pdf">docs/specifications.pdf</a>

## Exécuter l'application
```sh
mvn jetty:run
mvn jetty:run -Djetty.http.port=8080
```

## Automatisation des tests unitaires et du déploiement

Ce projet utilise <a href=".gitlab-ci.yml">GitLab CI</a> pour automatiser les tests unitaires et le déploiement d'une application. Le pipeline de déploiement comprend trois étapes principales : la construction, la dockerisation et le déploiement.

1. **Construction (build) :**
    - Cette étape compile, exécute les test unitaire et génère l'application à partir du code source.
    - Les dépendances Maven sont mises en cache pour accélérer les futures compilations.

2. **Dockerisation (dockerize) :**
    - Cette étape transforme l'application en une image Docker prête à être déployée.
    - Le service Docker-in-Docker (DinD) est utilisé pour exécuter les commandes Docker.
    - L'image Docker est construite à partir d'un fichier <a href="Dockerfile">Dockerfile</a> et poussée vers un registre Docker.

3. **Déploiement (deploy) :**
    - Cette étape déploie l'application sur un cluster Kubernetes.
    - Le contexte Kubernetes spécifié est utilisé pour la gestion du cluster.
    - Les déploiements et services existants liés à l'application sont supprimés.
    - Les manifestes Kubernetes situés dans le répertoire <a href="manifests">manifests</a> sont appliqués.

## GitLab Runner

Nous avons également mis en place notre propre GitLab Runner, installé sur une machine virtuelle Windows 10 et configuré en tant que `shell executor`. Pour préparer notre environnement et les dépendances requises de notre machine distante, vous trouverez dans le répertoire <a href="ansible">ansible</a> les scripts d'automatisation.  

## Agent GitLab pour Kubernetes

Pour connecter un cluster Kubernetes à GitLab, un <a href="https://gitlab.com/killeddb_kubernetes/k8s-agent">agent</a> doit être installé sur le cluster. Nous utilisons Google Kubernetes Engine, l'utilisation d'un cluster cloud permet d'obtenir une infrastructure évolutive, hautement disponible et facilement gérable pour exécuter vos applications, tout en offrant des avantages tels que la flexibilité, la sécurité et des économies de coûts.