package com.alphasquad.killeddb.utils;

import com.alphasquad.killeddb.core.storage.data.columns.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

    private static final ThreadLocal<Date> THREAD_LOCAL_DATE = ThreadLocal.withInitial(Date::new);
    private static final ThreadLocal<DateFormat> THREAD_LOCAL_DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));

    public static boolean parseBoolean(String value) {
        switch (value.toLowerCase(Locale.ROOT)) {
            case "true", "yes", "1" -> {
                return true;
            }
            default -> {
                return false;
            }
        }
    }

    public static Byte parseByte(String value) {
        return value.isEmpty() ? ColumnByte.DEFAULT_VALUE : Byte.parseByte(value);
    }

    public static char parseChar(String value) {
        return value.isEmpty() ? ColumnCharacter.DEFAULT_VALUE : value.charAt(0);
    }

    public static long parseDate(String value) {
        try {
            return THREAD_LOCAL_DATE_FORMAT.get().parse(value).getTime();
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }

        return ColumnDatetime.DEFAULT_VALUE;
    }

    public static String parseDate(long value) {
        if (value == ColumnDatetime.DEFAULT_VALUE) {
            return null;
        }

        THREAD_LOCAL_DATE.get().setTime(value);

        return THREAD_LOCAL_DATE_FORMAT.get().format(THREAD_LOCAL_DATE.get());
    }

    public static double parseDouble(String value) {
        return value.isEmpty() ? ColumnDouble.DEFAULT_VALUE : Double.parseDouble(value);
    }

    public static float parseFloat(String value) {
        return value.isEmpty() ? ColumnFloat.DEFAULT_VALUE : Float.parseFloat(value);
    }

    public static int parseInt(String value) {
        return value.isEmpty() ? ColumnInteger.DEFAULT_VALUE : Integer.parseInt(value);
    }

    public static long parseLong(String value) {
        return value.isEmpty() ? ColumnLong.DEFAULT_VALUE : Long.parseLong(value);
    }

    public static short parseShort(String value) {
        return value.isEmpty() ? ColumnShort.DEFAULT_VALUE : Short.parseShort(value);
    }

}
