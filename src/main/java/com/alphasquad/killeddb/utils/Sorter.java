package com.alphasquad.killeddb.utils;

import com.alphasquad.killeddb.core.storage.data.Table;
import com.alphasquad.killeddb.core.storage.data.columns.Column;

import java.util.List;
import java.util.Map;

public class Sorter {

    public static void sortList(Map<String, String> orderByColumnsMap, Table table, List<Map<String, Object>> list, Map<String, String> columnByAliases) {
        for (Map.Entry<String, String> entry : orderByColumnsMap.entrySet()) {
            String columnName = entry.getKey();
            String order = entry.getValue();

            Column<Class<?>> column = table.getColumns().get(columnName);
            if (column.isNumber()) {
                sortNumberList(list, columnName, order, columnByAliases);
            } else if (column.isString()) {
                sortStringList(list, columnName, order, columnByAliases);
            } else if (column.isChar()) {
                sortCharList(list, columnName, order, columnByAliases);
            }
        }
    }

    public static void sortNumberList(List<Map<String, Object>> list, String column, String order, Map<String, String> columnByAliases) {
        list.sort((o1, o2) -> {
            double o1D = ((Number) o1.getOrDefault(columnByAliases.get(column), o1.get(column))).doubleValue();
            double o2D = ((Number) o2.getOrDefault(columnByAliases.get(column), o2.get(column))).doubleValue();
            if (o1D == o2D) {
                return 0;
            }
            int dir = "asc".equals(order) ? 1 : -1;
            return (o1D < o2D ? -1 : 1) * dir;
        });
    }

    public static void sortStringList(List<Map<String, Object>> list, String column, String order, Map<String, String> columnByAliases) {
        list.sort((o1, o2) -> {
            String o1S = o1.getOrDefault(columnByAliases.get(column), o1.get(column)).toString();
            String o2S = o2.getOrDefault(columnByAliases.get(column), o2.get(column)).toString();
            int dir = "asc".equals(order) ? 1 : -1;
            return o1S.compareTo(o2S) * dir;
        });
    }

    public static void sortCharList(List<Map<String, Object>> list, String column, String order, Map<String, String> columnByAliases) {
        list.sort((o1, o2) -> {
            char o1C = (char) o1.getOrDefault(columnByAliases.get(column), o1.get(column));
            char o2C = (char) o2.getOrDefault(columnByAliases.get(column), o2.get(column));
            if (o1C == o2C) {
                return 0;
            }
            int dir = "asc".equals(order) ? 1 : -1;
            return (o1C < o2C ? -1 : 1) * dir;
        });
    }

}
