package com.alphasquad.killeddb.classes;

import com.alphasquad.killeddb.core.network.Node;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.utils.Splitter;

import java.util.*;

public class CSVTaskHandler {

    private String schemaName;
    private String tableName;

    private final Collection<Column<Class<?>>> columns;
    private final Map<Column<Class<?>>, Integer> columnsInCsvByIndex;
    private String columnsInCsvByIndexString;
    private final int columnsInCsvLength;
    private final char delimiter;

    private final Map<Column<Class<?>>, TempColumn> columnByTempColumn = new HashMap<>();
    private final Map<Node, List<String>> nodeBylines = new HashMap<>();

    private List<Node> nodes;
    private Node nextNode;
    private int nextNodeIndex;

    private int length = 0;
    private final int lengthToDistribute;

    private final boolean canDistribute;
    private int distributionLength = 0;

    public CSVTaskHandler(String schemaName, String tableName, Collection<Column<Class<?>>> columns, Map<Column<Class<?>>, Integer> columnsInCsvByIndex, String columnsInCsvByIndexString, int columnsInCsvSize, char delimiter, List<Node> nodes, int lengthToDistribute) {
        this.columns = columns;
        this.columnsInCsvByIndex = columnsInCsvByIndex;
        this.columnsInCsvLength = columnsInCsvSize;

        for (Column<Class<?>> column : this.columns) {
            this.columnByTempColumn.put(column, new TempColumn(column));
        }

        this.delimiter = delimiter;
        this.lengthToDistribute = lengthToDistribute;

        this.canDistribute = nodes.size() > 0;
        if (canDistribute) {
            this.schemaName = schemaName;
            this.tableName = tableName;
            this.columnsInCsvByIndexString = columnsInCsvByIndexString;

            this.nodes = nodes;
            this.nextNodeIndex = 0;
            this.nextNode = this.nodes.get(this.nextNodeIndex);

            if (this.nodes.size() > 1) {
                for (Node node : this.nodes) {
                    if (!node.isCurrent())
                        this.nodeBylines.put(node, new ArrayList<>());
                }
            }
        }
    }

    public void addLine(String line) {
        if (!this.canDistribute || this.nextNode.isCurrent()) {
            List<String> lines = Splitter.split(line, this.delimiter);
            if (lines.size() != this.columnsInCsvLength) {
                return;
            }

            for (Column<Class<?>> column : this.columns) {
                Integer index = this.columnsInCsvByIndex.get(column);
                this.columnByTempColumn.get(column).addValue((index == null) ? "" : lines.get(index).trim());
            }
        } else {
            List<String> lines = this.nodeBylines.get(this.nextNode);
            lines.add(line);
            this.length++;
            if (this.length == this.lengthToDistribute) {
                this.nextNode.insertLinesAsync(this.schemaName, this.tableName, this.delimiter, this.columnsInCsvByIndexString, lines);
                this.nodeBylines.put(this.nextNode, new ArrayList<>());
                this.distributionLength = (distributionLength == this.nodeBylines.size()) ? this.distributionLength + 1 : 0;
                this.length = 0;
            }
        }

        if (this.canDistribute) {
            this.nextNodeIndex = ((this.nextNodeIndex + 1) < this.nodes.size()) ? this.nextNodeIndex + 1 : 0;
            this.nextNode = this.nodes.get(this.nextNodeIndex);
        }
    }

    public void save() {
        for (TempColumn tempColumn : columnByTempColumn.values()) {
            tempColumn.merge();
        }

        if (this.canDistribute) {
            for (Map.Entry<Node, List<String>> entry : this.nodeBylines.entrySet())
                entry.getKey().insertLinesAsync(this.schemaName, this.tableName, this.delimiter, this.columnsInCsvByIndexString, entry.getValue());
        }
    }

}
