package com.alphasquad.killeddb.classes;

import lombok.Getter;

@Getter
public class JsonQueryRequest {

    private final String query;

    public JsonQueryRequest(String query) {
        this.query = query;
    }

}