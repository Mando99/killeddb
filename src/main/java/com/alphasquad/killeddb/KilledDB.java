package com.alphasquad.killeddb;

import com.alphasquad.killeddb.endpoints.APIEndpoint;
import com.alphasquad.killeddb.endpoints.NodeEndpoint;
import com.alphasquad.killeddb.exceptions.mappers.ColumnExceptionMapper;
import com.alphasquad.killeddb.exceptions.mappers.QueryExceptionMapper;
import com.alphasquad.killeddb.exceptions.mappers.SchemaExceptionMapper;
import com.alphasquad.killeddb.exceptions.mappers.TableExceptionMapper;
import com.alphasquad.killeddb.providers.GsonProvider;
import org.jboss.resteasy.plugins.interceptors.encoding.AcceptEncodingGZIPFilter;
import org.jboss.resteasy.plugins.interceptors.encoding.GZIPDecodingInterceptor;
import org.jboss.resteasy.plugins.interceptors.encoding.GZIPEncodingInterceptor;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.*;

@ApplicationPath("")
public class KilledDB extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(GsonProvider.class);
        classes.add(AcceptEncodingGZIPFilter.class);
        classes.add(GZIPDecodingInterceptor.class);
        classes.add(GZIPEncodingInterceptor.class);
        classes.add(ColumnExceptionMapper.class);
        classes.add(QueryExceptionMapper.class);
        classes.add(SchemaExceptionMapper.class);
        classes.add(TableExceptionMapper.class);

        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<>();

        singletons.add(new APIEndpoint());
        singletons.add(new NodeEndpoint());

        return singletons;
    }

}