package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.core.network.Node;
import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.core.network.managers.NodeManager;
import com.alphasquad.killeddb.core.network.managers.TaskManager;
import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.core.storage.data.Table;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.enums.SQLAggregateFunctions;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.QueryException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import com.alphasquad.killeddb.factories.ColumnFactory;
import com.alphasquad.killeddb.utils.Parser;
import com.alphasquad.killeddb.utils.Sorter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.alphasquad.killeddb.utils.Sorter.sortList;

public class TaskExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskExecutorService.class);

    private final Gson gson = new GsonBuilder().serializeNulls().create();

    public Object execute(Task task) throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        return this.execute(task, false);
    }

    public Object execute(Task task, boolean distribute) throws QueryException, SchemaException, TableException, ColumnException, ExecutionException, InterruptedException {
        Object obj = null;

        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();
        LOGGER.info("DATA MAP: " + dataMap);

        String schemaName = (String) dataMap.get(TaskDataMapKeys.SCHEMA_NAME);
        String tableName = (String) dataMap.get(TaskDataMapKeys.TABLE_NAME);

        switch (task.getTaskName()) {
            case CREATE_SCHEMA -> obj = MemoryDB.createSchema(new Schema(schemaName));
            case RENAME_SCHEMA ->
                    obj = MemoryDB.renameSchema(schemaName, (String) dataMap.get(TaskDataMapKeys.NEW_SCHEMA_NAME));
            case DROP_SCHEMA -> obj = MemoryDB.dropSchema(schemaName);
            case CREATE_TABLE ->
                    obj = this.executeCreateTable(schemaName, tableName, (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS));
            case RENAME_TABLE ->
                    obj = MemoryDB.renameTable(schemaName, tableName, (String) dataMap.get(TaskDataMapKeys.NEW_TABLE_NAME));
            case DROP_TABLE -> obj = MemoryDB.dropTable(schemaName, tableName);
            case ADD_COLUMNS ->
                    obj = this.executeAddColumns(schemaName, tableName, (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_TYPES));
            case CHANGE_COLUMNS ->
                    obj = this.executeChangeColumns(schemaName, tableName, (Map<String, Map<String, String>>) dataMap.get(TaskDataMapKeys.COLUMN_BY_COLUMNS_AND_TYPES));
            case DROP_COLUMNS ->
                    obj = this.executeDropColumns(schemaName, tableName, (String[]) dataMap.get(TaskDataMapKeys.COLUMNS));
            case INSERT_DATA -> obj = this.executeInsertData(task, distribute);
            case SELECT_DATA -> obj = this.executeSelectData(task, distribute);
            case UPDATE_DATA -> obj = this.executeUpdateData(task, distribute);
            case DELETE_DATA -> obj = this.executeDeleteData(task, distribute);
        }

        return obj;
    }

    private Object executeCreateTable(String schemaName, String tableName, Map<String, String> columnNameByType) throws QueryException, SchemaException, TableException {
        Table table = MemoryDB.createTable(schemaName, tableName);

        for (Map.Entry<String, String> entry : columnNameByType.entrySet()) {
            table.addColumn(ColumnFactory.createColumn(entry.getKey(), SQLQueryService.getParsedType(entry.getValue())));
        }

        return table;
    }

    private Object executeAddColumns(String schemaName, String tableName, Map<String, String> columnByTypeMap) throws QueryException, SchemaException, TableException, ColumnException {
        List<Column<Class<?>>> columns = new ArrayList<>();

        for (Map.Entry<String, String> entry : columnByTypeMap.entrySet()) {
            Column<Class<?>> column = ColumnFactory.createColumn(entry.getKey(), SQLQueryService.getParsedType(entry.getValue()));
            if (column != null)
                columns.add(MemoryDB.addColumn(schemaName, tableName, column));
        }

        return columns.size() > 0 ? columns : null;
    }

    private Object executeChangeColumns(String schemaName, String tableName, Map<String, Map<String, String>> columnByColumnsAndTypes) throws QueryException, SchemaException, TableException, ColumnException {
        for (Map.Entry<String, Map<String, String>> entry : columnByColumnsAndTypes.entrySet()) {
            String columnName = entry.getKey();

            for (Map.Entry<String, String> entry2 : entry.getValue().entrySet()) {
                String newColumnName = entry2.getKey();
                ColumnTypes newColumnTypes = SQLQueryService.getParsedType(entry2.getValue());

                MemoryDB.changeColumn(schemaName, tableName, columnName, newColumnName, newColumnTypes);
            }
        }

        return columnByColumnsAndTypes;
    }

    private Object executeDropColumns(String schemaName, String tableName, String[] columnNames) throws SchemaException, TableException {
        for (String columnName : columnNames) {
            MemoryDB.dropColumn(schemaName, tableName, columnName);
        }

        return columnNames;
    }

    private Object executeInsertData(Task task, boolean distribute) throws SchemaException, TableException, ColumnException {
        String schemaName = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableName = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Table table = MemoryDB.getTable(schemaName, tableName);

        List<String> columnsHeaders = (List<String>) task.getDataMap().get(TaskDataMapKeys.COLUMNS);
        List<Map<String, String>> columnValues = (List<Map<String, String>>) task.getDataMap().get(TaskDataMapKeys.COLUMNS_VALUES);

        if (distribute && TaskManager.get().canDistribute()) {
            NodeManager nodeManager = NodeManager.get();
            Map<Node, List<Map<String, String>>> nodeByColumnValues = new HashMap<>();

            for (Map<String, String> columnByValues : columnValues) {
                nodeByColumnValues.computeIfAbsent(nodeManager.getNextNode(), k -> new ArrayList<>()).add(columnByValues);
            }

            for (Map.Entry<Node, List<Map<String, String>>> entry : nodeByColumnValues.entrySet()) {
                Node node = entry.getKey();
                if (node.isCurrent()) {
                    this.insertData(columnValues, table, schemaName, columnsHeaders);
                } else {
                    task.addData(TaskDataMapKeys.COLUMNS_VALUES, entry.getValue());
                    node.executeTaskAsync(task);
                }
            }
        } else {
            this.insertData(columnValues, table, schemaName, columnsHeaders);
        }

        return table.getColumns();
    }

    private void insertData(List<Map<String, String>> rows, Table table, String schemaName, List<String> columnsHeaders) throws SchemaException, TableException, ColumnException {
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaName, table.getName());

        for (Map<String, String> row : rows) {
            for (String column : columns.keySet()) {
                MemoryDB.getColumn(schemaName, table.getName(), column).addValue((columnsHeaders.contains(column)) ? row.get(column) : "");
            }
        }

        table.setSize(table.getSize() + rows.size());
    }


    private Object executeSelectData(Task task, boolean distribute) throws SchemaException, TableException, QueryException, ExecutionException, InterruptedException, ColumnException {
        String schemaName = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableName = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        Table table = MemoryDB.getTable(schemaName, tableName);
        Map<String, Column<Class<?>>> columns = table.getColumns();

        List<String> whereAndOrConditions = (List<String>) dataMap.get(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS);
        List<String> havingConditions = (List<String>) dataMap.get(TaskDataMapKeys.HAVING);

        List<String> selectedColumns = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS);
        Map<String, String> aggregatesStr = (Map<String, String>) dataMap.get(TaskDataMapKeys.AGGREGATES);

        Map<String, SQLAggregateFunctions> aggregates = new HashMap<>();
        for (Map.Entry<String, String> aggregatesEntry : aggregatesStr.entrySet()) {
            aggregates.put(aggregatesEntry.getKey(), SQLQueryService.getSQLAggregateFunction(aggregatesEntry.getValue()));
        }

        List<String> selectedColumnsAndAggregates = (List<String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_AGGREGATES);
        Map<String, String> columnByAliases = (Map<String, String>) dataMap.get(TaskDataMapKeys.ALIASES);
        Map<String, String> aliasesByColumn = new HashMap<>();
        for (Map.Entry<String, String> aliasesEntry : columnByAliases.entrySet()) {
            aliasesByColumn.put(aliasesEntry.getValue(), aliasesEntry.getKey());
        }

        List<String> groupBy = (List<String>) dataMap.get(TaskDataMapKeys.GROUP_BY);
        Map<String, String> orderBy = (Map<String, String>) dataMap.get(TaskDataMapKeys.ORDER_BY);

        String limitStr = (String) dataMap.get(TaskDataMapKeys.LIMIT);
        Integer limit = null;
        if (limitStr != null) {
            limit = Integer.parseInt(limitStr);
        }

        List<String> dateColumnsOrDateAggregates = new ArrayList<>();
        for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
            String columnName;
            if (selectedColumnOrAgg.contains("(")) {
                if (selectedColumnOrAgg.contains("count")) {
                    continue;
                }

                columnName = StringUtils.substringBetween(selectedColumnOrAgg, "(", ")");
                if (columnName.contains("distinct")) {
                    columnName = StringUtils.substringAfter(columnName, "distinct").trim();
                }
            } else {
                columnName = selectedColumnOrAgg;
            }

            Column<Class<?>> column = columns.get(columnName);
            if (column == null) {
                throw new ColumnException("Column does not exist.");
            }

            if (column.isDate()) {
                dateColumnsOrDateAggregates.add(columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg));
            }
        }

        List<Map<String, Object>> rows = new ArrayList<>();
        Map<String, Object> aggResults = new HashMap<>();

        boolean onlyAggregates = whereAndOrConditions == null && !aggregates.isEmpty() && groupBy == null;
        if (onlyAggregates) {
            for (Map.Entry<String, SQLAggregateFunctions> aggEntry : aggregates.entrySet()) {
                String agg = aggEntry.getKey();
                SQLAggregateFunctions sqlAggregateFunctions = aggEntry.getValue();
                Column<Class<?>> column = columns.get(StringUtils.substringBetween(agg, "(", ")"));

                if (!selectedColumns.isEmpty()) {
                    throw new QueryException("Missing group by clause.");
                }

                switch (sqlAggregateFunctions) {
                    case COUNT -> aggResults.put(agg, column.getCount());
                    case COUNT_ALL -> aggResults.put(agg, table.getSize());
                    case COUNT_DISTINCT -> {
                        column = table.getColumns().get(StringUtils.substringBetween(agg, "(", ")").split(" ")[1].trim());
                        aggResults.put(agg, column.getCountDistinct());
                    }
                    case MIN -> aggResults.put(agg, column.getMin());
                    case MAX -> aggResults.put(agg, column.getMax());
                    case SUM -> aggResults.put(agg, column.getSum());
                    case AVG -> aggResults.put(agg, column.getAvg());
                }
            }

            if (selectedColumns.isEmpty()) {
                Map<String, Object> row = new LinkedHashMap<>();
                for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
                    row.put(columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg), aggResults.get(selectedColumnOrAgg));
                }

                rows.add(row);
            }

        } else {
            for (int rowIndex = 0; rowIndex < table.getSize(); rowIndex++) {
                if (this.checkWhereAndOrConditions(columns, whereAndOrConditions, rowIndex)) {
                    Map<String, Object> row = new LinkedHashMap<>();

                    for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
                        if (groupBy != null || selectedColumns.isEmpty()) {
                            if (selectedColumnOrAgg.contains("(")) {
                                if (selectedColumnOrAgg.equals("count(*)")) {
                                    continue;
                                }

                                String columnName = StringUtils.substringBetween(selectedColumnOrAgg, "(", ")");
                                if (columnName.contains("distinct")) {
                                    columnName = StringUtils.substringAfter(columnName, "distinct").trim();
                                }

                                row.put(columnByAliases.getOrDefault(columnName, columnName), columns.get(columnName).getValue(rowIndex));
                            } else {
                                row.put(columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg), columns.get(selectedColumnOrAgg).getValue(rowIndex));
                            }
                        } else {
                            row.put(columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg), columns.get(selectedColumnOrAgg).getValue(rowIndex));
                        }
                    }

                    rows.add(row);
                }

                if (whereAndOrConditions == null && groupBy == null && limit != null && limit == rowIndex + 1) {
                    distribute = false;
                    break;
                }
            }
        }

        List<Future<Response>> responses = null;
        if (distribute && TaskManager.get().canDistribute()) {
            responses = TaskManager.get().distributeAsyncTaskToOtherNodes(task);
        }

        if (whereAndOrConditions != null && !aggregates.isEmpty() && groupBy == null) {
            for (Map.Entry<String, SQLAggregateFunctions> aggEntry : aggregates.entrySet()) {
                String agg = aggEntry.getKey();
                String columnName = StringUtils.substringBetween(agg, "(", ")");
                if (columnName.contains("distinct")) {
                    columnName = StringUtils.substringAfter(columnName, "distinct");
                }

                Column<Class<?>> column = table.getColumns().get(columnName);
                SQLAggregateFunctions sqlAggregateFunctions = aggEntry.getValue();
                String aliasAgg = columnByAliases.getOrDefault(agg, agg);
                String aliasColumn = columnByAliases.getOrDefault(columnName, columnName);

                switch (sqlAggregateFunctions) {
                    case COUNT -> aggResults.put(aliasAgg, column.getCount(rows, aliasColumn));
                    case COUNT_ALL -> aggResults.put(aliasAgg, rows.size());
                    case COUNT_DISTINCT -> {
                        column = table.getColumns().get(StringUtils.substringBetween(columnName, "(", ")").split(" ")[1].trim());
                        aggResults.put(aliasAgg, column.getCountDistinct(rows, aliasColumn));
                    }
                    case AVG -> aggResults.put(aliasAgg, column.getAvg(rows, aliasColumn));
                    case SUM -> aggResults.put(aliasAgg, column.getSum(rows, aliasColumn));
                    case MIN -> aggResults.put(aliasAgg, column.getMin(rows, aliasColumn));
                    case MAX -> aggResults.put(aliasAgg, column.getMax(rows, aliasColumn));
                }
            }

            if (selectedColumns.isEmpty()) {
                return List.of(aggResults);
            }

            rows.add(0, aggResults);
        }

        Function<Map<String, Object>, Map<String, Object>> compositeKey = null;
        if (groupBy != null) {
            if (!new HashSet<>(selectedColumns).equals(new HashSet<>(groupBy))) {
                throw new QueryException("Error: Not a GROUP BY expression");
            }

            compositeKey = row -> {
                Map<String, Object> mapGroupBy = new HashMap<>();
                for (String groupByColumn : groupBy) {
                    mapGroupBy.put(columnByAliases.getOrDefault(groupByColumn, groupByColumn), row.get(columnByAliases.getOrDefault(groupByColumn, groupByColumn)));
                }

                return mapGroupBy;
            };

            if (aggregates.isEmpty()) {
                rows = rows.parallelStream().map(compositeKey).distinct().collect(Collectors.toList());
            } else {
                Map<Map<String, Object>, Map<String, Object>> result = rows
                        .parallelStream()
                        .collect(Collectors.groupingByConcurrent(compositeKey, Collectors.collectingAndThen(Collectors.toList(), rowsListMap -> {
                            Map<String, Object> aggResult = new HashMap<>();

                            for (Map.Entry<String, SQLAggregateFunctions> aggEntry : aggregates.entrySet()) {
                                String agg = aggEntry.getKey();
                                if (agg.equals("count(*)")) {
                                    aggResult.put(columnByAliases.getOrDefault(agg, agg), rowsListMap.size());
                                    continue;
                                }
                                String columnName = StringUtils.substringBetween(agg, "(", ")");
                                if (columnName.contains("distinct")) {
                                    columnName = StringUtils.substringAfter(columnName, "distinct").trim();
                                }

                                Column<Class<?>> column = table.getColumns().get(columnName);
                                SQLAggregateFunctions sqlAggregateFunctions = aggEntry.getValue();
                                String aliasAgg = columnByAliases.getOrDefault(agg, agg);
                                String aliasColumn = columnByAliases.getOrDefault(columnName, columnName);

                                switch (sqlAggregateFunctions) {
                                    case COUNT -> aggResult.put(aliasAgg, column.getCount(rowsListMap, aliasColumn));
                                    case COUNT_ALL -> aggResult.put(aliasAgg, rowsListMap.size());
                                    case COUNT_DISTINCT -> {
                                        column = table.getColumns().get(columnName);
                                        aggResult.put(aliasAgg, column.getCountDistinct(rowsListMap, aliasColumn));
                                    }
                                    case AVG -> aggResult.put(aliasAgg, column.getAvg(rowsListMap, aliasColumn));
                                    case SUM -> aggResult.put(aliasAgg, column.getSum(rowsListMap, aliasColumn));
                                    case MIN -> aggResult.put(aliasAgg, column.getMin(rowsListMap, aliasColumn));
                                    case MAX -> aggResult.put(aliasAgg, column.getMax(rowsListMap, aliasColumn));
                                }
                            }

                            return aggResult;
                        })));

                rows = new ArrayList<>();
                for (Map.Entry<Map<String, Object>, Map<String, Object>> entry : result.entrySet()) {
                    if (checkHavingConditions(columns, entry, havingConditions, columnByAliases)) {
                        Map<String, Object> row = new LinkedHashMap<>();
                        for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
                            String alias = columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg);
                            if (aggregates.get(selectedColumnOrAgg) != null) {
                                row.put(alias, entry.getValue().get(alias));
                            } else {
                                row.put(alias, entry.getKey().get(alias));
                            }
                        }
                        rows.add(row);
                    }
                }
            }
        }

        if (distribute) {
            if (responses != null) {
                for (Future<Response> response : responses) {
                    List<Map<String, String>> rowsResponse = this.gson.fromJson(response.get().readEntity(String.class), new TypeToken<List<Map<String, String>>>() {
                    }.getType());
                    if (rowsResponse != null && rowsResponse.size() > 0) {
                        if (onlyAggregates) {
                            for (Map.Entry<String, SQLAggregateFunctions> aggEntry : aggregates.entrySet()) {
                                String agg = aggEntry.getKey();
                                SQLAggregateFunctions sqlAggregateFunctions = aggEntry.getValue();
                                Column<Class<?>> column = columns.get(StringUtils.substringBetween(agg, "(", ")"));

                                Object currentNodeResult = aggResults.get(agg);
                                String nodeValue = rowsResponse.get(0).get(columnByAliases.getOrDefault(agg, agg));

                                switch (sqlAggregateFunctions) {
                                    case COUNT, COUNT_ALL ->
                                            aggResults.put(agg, (long) currentNodeResult + Parser.parseLong(nodeValue));
                                    case COUNT_DISTINCT -> {
                                        column = table.getColumns().get(StringUtils.substringBetween(agg, "(", ")").split(" ")[1].trim());
                                        aggResults.put(agg, column.getCountDistinct((long) currentNodeResult, nodeValue));
                                    }
                                    case MIN -> aggResults.put(agg, column.getMin(currentNodeResult, nodeValue));
                                    case MAX -> aggResults.put(agg, column.getMax(currentNodeResult, nodeValue));
                                    case SUM -> aggResults.put(agg, column.getSum(currentNodeResult, nodeValue));
                                    case AVG -> aggResults.put(agg, column.getAvg(currentNodeResult, nodeValue));
                                }
                            }

                            if (selectedColumns.isEmpty()) {
                                Map<String, Object> row = new LinkedHashMap<>();
                                for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
                                    row.put(columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg), aggResults.get(selectedColumnOrAgg));
                                }

                                rows.add(row);
                            }
                        } else {
                            for (Map<String, String> nodeRow : rowsResponse) {
                                Map<String, Object> parsedRow = new LinkedHashMap<>();

                                for (Map.Entry<String, String> rowEntry : nodeRow.entrySet()) {
                                    String columnName = columns.get(rowEntry.getKey()) != null ? rowEntry.getKey() : aliasesByColumn.getOrDefault(rowEntry.getKey(), rowEntry.getKey());

                                    if (columnName.contains("(")) {
                                        if (columnName.contains("count") || columnName.contains("distinct")) {
                                            parsedRow.put(rowEntry.getKey(), Long.parseLong(rowEntry.getValue()));
                                        } else {
                                            columnName = StringUtils.substringBetween(columnName, "(", ")");
                                            parsedRow.put(rowEntry.getKey(), columns.get(columnName).getParsedValue(rowEntry.getValue()));
                                        }
                                    } else {
                                        parsedRow.put(rowEntry.getKey(), columns.get(columnName).getParsedValue(rowEntry.getValue()));
                                    }
                                }

                                rows.add(parsedRow);
                            }
                        }
                    }
                }
                if (groupBy != null) {
                    if (aggregates.isEmpty()) {
                        rows = rows.parallelStream().map(compositeKey).distinct().collect(Collectors.toList());
                    } else {

                        Map<Map<String, Object>, Map<String, Object>> result = rows
                                .parallelStream()
                                .collect(Collectors.groupingByConcurrent(compositeKey, Collectors.collectingAndThen(Collectors.toList(), rowsListMap -> {
                                    Map<String, Object> aggResult = new HashMap<>();

                                    for (Map.Entry<String, SQLAggregateFunctions> aggEntry : aggregates.entrySet()) {
                                        String agg = aggEntry.getKey();

                                        String columnName = StringUtils.substringBetween(agg, "(", ")");
                                        if (columnName.contains("distinct")) {
                                            columnName = StringUtils.substringAfter(columnName, "distinct").trim();
                                        }

                                        Column<Class<?>> column = table.getColumns().get(columnName);
                                        SQLAggregateFunctions sqlAggregateFunctions = aggEntry.getValue();
                                        String aliasAgg = columnByAliases.getOrDefault(agg, agg);
                                        switch (sqlAggregateFunctions) {
                                            case COUNT ->
                                                    aggResult.put(aliasAgg, column.getCount(rowsListMap, aliasAgg));
                                            case COUNT_ALL -> {
                                                long size = 0L;
                                                for (Map<String, Object> r : rowsListMap) {
                                                    size += ((Number) r.get(aliasAgg)).longValue();
                                                }
                                                aggResult.put(aliasAgg, size);
                                            }
                                            case COUNT_DISTINCT -> {
                                                column = table.getColumns().get(columnName);
                                                aggResult.put(aliasAgg, column.getCountDistinct(rowsListMap, aliasAgg));
                                            }
                                            case AVG -> aggResult.put(aliasAgg, column.getAvg(rowsListMap, aliasAgg));
                                            case SUM -> aggResult.put(aliasAgg, column.getSum(rowsListMap, aliasAgg));
                                            case MIN -> aggResult.put(aliasAgg, column.getMin(rowsListMap, aliasAgg));
                                            case MAX -> aggResult.put(aliasAgg, column.getMax(rowsListMap, aliasAgg));
                                        }

                                    }

                                    return aggResult;
                                })));

                        rows = new ArrayList<>();
                        for (Map.Entry<Map<String, Object>, Map<String, Object>> entry : result.entrySet()) {
                            if (checkHavingConditions(columns, entry, havingConditions, columnByAliases)) {
                                Map<String, Object> row = new LinkedHashMap<>();
                                for (String selectedColumnOrAgg : selectedColumnsAndAggregates) {
                                    String alias = columnByAliases.getOrDefault(selectedColumnOrAgg, selectedColumnOrAgg);
                                    if (aggregates.get(selectedColumnOrAgg) != null) {
                                        row.put(alias, entry.getValue().get(alias));
                                    } else {
                                        row.put(alias, entry.getKey().get(alias));
                                    }
                                }
                                rows.add(row);
                            }
                        }
                    }
                }
            }



            for (String dateColumnOrDataAgg : dateColumnsOrDateAggregates) {
                rows.parallelStream().forEach(row -> row.put(dateColumnOrDataAgg, Parser.parseDate((long) row.get(dateColumnOrDataAgg))));
            }

            if (orderBy != null) {
                if (!new HashSet<>(selectedColumns).containsAll(orderBy.keySet())) {
                    throw new QueryException("selected columns don't match order by columns.");
                }
                Sorter.sortList(orderBy, table, rows, columnByAliases);
            }

            limit = (limitStr == null) ? rows.size() : Math.min(rows.size(), Integer.parseInt(limitStr));
            rows = rows.subList(0, limit);
        }

        return rows;
    }

    private Object executeUpdateData(Task task, boolean distribute) throws SchemaException, TableException {
        String schemaName = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableName = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        if (dataMap.get(TaskDataMapKeys.DELETE_ALL_DATA) != null) {
            return MemoryDB.deleteAllData(schemaName, tableName);
        }

        Table table = MemoryDB.getTable(schemaName, tableName);
        Map<String, Column<Class<?>>> columns = table.getColumns();

        Map<String, String> columnByNewValueMap = (Map<String, String>) dataMap.get(TaskDataMapKeys.COLUMNS_AND_NEW_VALUES);
        List<String> whereAndOrConditions = (List<String>) dataMap.get(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS);

        int rowIndex = 0;
        while (rowIndex < table.getSize()) {
            if (this.checkWhereAndOrConditions(columns, whereAndOrConditions, rowIndex)) {
                for (Map.Entry<String, String> entry : columnByNewValueMap.entrySet()) {
                    columns.get(entry.getKey()).setValue(rowIndex, entry.getValue());
                }
            }
            rowIndex++;
        }

        if (distribute) {
            TaskManager.get().distributeAsyncTaskToOtherNodes(task);
        }

        return "Rows have been successfully updated.";
    }

    private Object executeDeleteData(Task task, boolean distribute) throws SchemaException, TableException {
        String schemaName = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableName = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<TaskDataMapKeys, Object> dataMap = task.getDataMap();

        if (dataMap.get(TaskDataMapKeys.DELETE_ALL_DATA) != null) {
            return MemoryDB.deleteAllData(schemaName, tableName);
        }

        Table table = MemoryDB.getTable(schemaName, tableName);
        Map<String, Column<Class<?>>> columns = table.getColumns();

        List<String> whereAndOrConditions = (List<String>) dataMap.get(TaskDataMapKeys.WHERE_AND_OR_CONDITIONS);

        int rowIndex = 0;
        while (rowIndex < table.getSize()) {
            if (this.checkWhereAndOrConditions(columns, whereAndOrConditions, rowIndex)) {
                for (Map.Entry<String, Column<Class<?>>> entry : columns.entrySet()) {
                    entry.getValue().getValues().remove(rowIndex);
                }

                table.decrementSize();
            } else {
                rowIndex++;
            }
        }

        if (distribute) {
            TaskManager.get().distributeAsyncTaskToOtherNodes(task);
        }

        return "Rows have been successfully deleted.";
    }


    private boolean checkWhereAndOrConditions(Map<String, Column<Class<?>>> columns, List<String> conditions, int rowIndex) {
        if (conditions == null) {
            return true;
        }

        int conditionsSize = conditions.size();
        Column<Class<?>> column;
        String operator;
        String compValue;
        boolean result = false;
        int i = 2;
        while (i < conditionsSize) {
            column = columns.get(conditions.get(i - 2));
            operator = conditions.get(i - 1);
            compValue = conditions.get(i);

            result = this.checkCondition(column, column.getValue(rowIndex), operator, compValue);
            if ((i + 1) < conditionsSize && result && conditions.get(i + 1).equals("or")) {
                break;
            }

            i += 4;
        }

        return result;
    }

    private boolean checkHavingConditions(Map<String, Column<Class<?>>> columns, Map.Entry<Map<String, Object>, Map<String, Object>> columnsAndAggregates, List<String> havingConditions, Map<String, String> aliases) throws QueryException {
        if (havingConditions == null) {
            return true;
        }

        int havingConditionsSize = havingConditions.size();
        Column<Class<?>> column = null;
        String element;
        String operator;
        String compValue;
        Object value;
        boolean result = false;
        int i = 2;

        while (i < havingConditionsSize) {
            element = havingConditions.get(i - 2);
            operator = havingConditions.get(i - 1);
            compValue = havingConditions.get(i);

            if (element.contains("(")) {
                value = columnsAndAggregates.getValue().get(aliases.getOrDefault(element, element));
            } else {
                value = columnsAndAggregates.getKey().get(aliases.getOrDefault(element, element));
                column = columns.get(element);
            }

            if (value == null) {
                throw new QueryException("Error: Not a HAVING expression");
            }

            result = element.contains("(")
                    ? this.checkHavingAggCondition(value, operator, compValue)
                    : this.checkCondition(column, value, operator, compValue);
            if ((i + 1) < havingConditionsSize && result && havingConditions.get(i + 1).equals("or")) {
                break;
            }
            i += 4;
        }

        return result;
    }

    private boolean checkCondition(Column<Class<?>> column, Object value, String operator, String compValue) {
        switch (operator) {
            case "=" -> {
                return column.equalValues(value, compValue);
            }
            case "!=", "<>" -> {
                return !column.equalValues(value, compValue);
            }
            case "<" -> {
                return column.lowerValues(value, compValue);
            }
            case ">" -> {
                return column.higherValues(value, compValue);
            }
            case "<=" -> {
                return column.lowerOrEqualValues(value, compValue);
            }
            case ">=" -> {
                return column.higherOrEqualValues(value, compValue);
            }
            case "like" -> {
                compValue = compValue.replaceAll("'", "").replaceAll("\"", "");
                int countJoker = StringUtils.countMatches(compValue, "%");
                if (countJoker == 2) {
                    return column.containValues(value, StringUtils.substringBetween(compValue, "%", "%"));
                } else if (countJoker == 1) {
                    String afterJoker = StringUtils.substringAfter(compValue, "%");
                    String beforeJoker = StringUtils.substringBefore(compValue, "%");
                    return checkLikeCondition(column, value, afterJoker, beforeJoker);
                }
            }
        }

        return false;
    }

    private boolean checkHavingAggCondition(Object value, String operator, String compValue) {
        switch (operator) {
            case "=" -> {
                return ((Number) value).doubleValue() == Parser.parseDouble(compValue);
            }
            case "!=", "<>" -> {
                return ((Number) value).doubleValue() != Parser.parseDouble(compValue);
            }
            case "<" -> {
                return ((Number) value).doubleValue() < Parser.parseDouble(compValue);
            }
            case ">" -> {
                return ((Number) value).doubleValue() > Parser.parseDouble(compValue);
            }
            case "<=" -> {
                return ((Number) value).doubleValue() <= Parser.parseDouble(compValue);
            }
            case ">=" -> {
                return ((Number) value).doubleValue() >= Parser.parseDouble(compValue);
            }
        }
        return false;
    }

    private boolean checkLikeCondition(Column<Class<?>> column, Object value, String substringAfter, String substringBefore) {
        if (substringAfter.isEmpty()) {
            return column.startWithValues(value, substringBefore);
        }

        if (substringBefore.isEmpty()) {
            return column.endWithValues(value, substringAfter);
        }

        return column.startWithValues(value, substringBefore) && column.endWithValues(value, substringAfter);
    }

}


