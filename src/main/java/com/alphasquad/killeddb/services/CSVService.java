package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.classes.TempColumn;
import com.alphasquad.killeddb.core.network.Node;
import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.core.network.managers.NodeManager;
import com.alphasquad.killeddb.core.network.managers.TaskManager;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.TableException;
import com.alphasquad.killeddb.classes.CSVTaskHandler;
import com.alphasquad.killeddb.utils.Splitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.core.storage.data.Table;
import com.alphasquad.killeddb.utils.Timer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class CSVService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CSVService.class);

    public String load(TaskNames taskName, String schemaName, String tableName, InputStream inputStream, String delimiter, long skip, long limit) throws Exception {
        LOGGER.info("Start of upload...");

        Timer timer = new Timer();
        timer.start();

        if (taskName == TaskNames.UPDATE_DATA) {
            MemoryDB.deleteAllData(schemaName, tableName);
            Task deleteAllDataTask = new Task(TaskNames.DELETE_DATA);
            deleteAllDataTask.addData(TaskDataMapKeys.DELETE_ALL_DATA, "true");
            TaskManager.get().distributeAsyncTaskToOtherNodes(deleteAllDataTask);
        }

        Table table = MemoryDB.getTable(schemaName, tableName);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            Map<String, Column<Class<?>>> columns = table.getColumns();
            StringBuilder columnsInCsvByIndexBuilder = new StringBuilder();
            Map<Column<Class<?>>, Integer> columnsInCsvByIndex = new HashMap<>();

            int columnsInCsvSize = 0;
            for (int i = 0; i <= skip; i++) {
                String headerLine = br.readLine();

                if (i < skip) { continue; }

                String[] csvColumns = headerLine.toLowerCase().split(delimiter);
                for (int j = 0; j < csvColumns.length; j++) {
                    String columnName = csvColumns[j].trim();
                    Column<Class<?>> columnInTable = columns.get(columnName);

                    if (columnInTable != null) {
                        columnsInCsvByIndexBuilder.append(columnName).append(" ").append(j).append(delimiter);
                        columnsInCsvByIndex.put(columnInTable, j);
                        columnsInCsvSize++;
                    }
                }
            }

            if (columnsInCsvSize == 0) {
                throw new ColumnException("The csv columns do not exist in the table.");
            }

            String columnInCsvString = columnsInCsvByIndexBuilder.toString();
            int finalColumnsInCsvSize = columnsInCsvSize;
            List<Node> nodes = NodeManager.get().getActiveNodes();
            char delimiterToChar = delimiter.charAt(0);

            Map<Thread, CSVTaskHandler> threadByCSVTaskHandler = new ConcurrentHashMap<>();
            (limit == 0 ? br.lines() : br.lines().limit(limit))
                .unordered()
                .parallel()
                .forEach(line -> threadByCSVTaskHandler.computeIfAbsent(Thread.currentThread(), k -> new CSVTaskHandler(schemaName, tableName, columns.values(), columnsInCsvByIndex, columnInCsvString, finalColumnsInCsvSize, delimiterToChar, nodes, 300_000)).addLine(line));

            for (CSVTaskHandler taskHandler : threadByCSVTaskHandler.values()) {
                taskHandler.save();
            }

            table.setSizeAuto();

            timer.stop();

            LOGGER.info("End of upload : lines inserted in " + timer.getTime() + " ms.");

            return "The lines have been successfully inserted.\nTime: " + timer.getTime() + " ms.";
        }
    }

    public String load(String schemaName, String tableName, String delimiter, String columnsByIndexString, List<String> lines) throws SchemaException, TableException {
        LOGGER.info("Start of upload...");

        Timer timer = new Timer();
        timer.start();

        Table table = MemoryDB.getTable(schemaName, tableName);
        Map<String, Column<Class<?>>> columns = table.getColumns();

        Map<Column<Class<?>>, Integer> columnsByIndex = new HashMap<>();
        for (String columnAndIndex : columnsByIndexString.split(delimiter)) {
            String[] columnAndIndexSplit = columnAndIndex.split(" ");
            columnsByIndex.put(columns.get(columnAndIndexSplit[0]), Integer.parseInt(columnAndIndexSplit[1]));
        }

        Map<Thread, Map<Column<Class<?>>, TempColumn>> threadAndColumnMap = new ConcurrentHashMap<>();

        char delimiterToChar = delimiter.charAt(0);
        lines.stream().unordered().parallel().forEach(line -> {
            List<String> lineSplit = Splitter.split(line, delimiterToChar);
            for (Column<Class<?>> column : columns.values()) {
                Integer index = columnsByIndex.get(column);
                threadAndColumnMap.computeIfAbsent(Thread.currentThread(), k -> new HashMap<>()).computeIfAbsent(column, k -> new TempColumn(column)).addValue((index == null) ? "" : lineSplit.get(index).trim());
            }
        });

        for (Map<Column<Class<?>>, TempColumn> columnMap : threadAndColumnMap.values()) {
            for (TempColumn threadTempColumn : columnMap.values())
                threadTempColumn.merge();
        }

        table.setSizeAuto();

        timer.stop();

        LOGGER.info("End of upload : lines inserted in " + timer.getTime() + " ms.");

        return "The lines have been successfully inserted.";
    }

}
