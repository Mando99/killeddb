package com.alphasquad.killeddb.endpoints;

import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.core.network.managers.TaskManager;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.classes.JsonQueryRequest;
import com.alphasquad.killeddb.services.CSVService;
import com.alphasquad.killeddb.services.SQLQueryService;
import com.alphasquad.killeddb.services.TaskExecutorService;
import org.jboss.resteasy.annotations.GZIP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.EnumSet;

@Path("api")
@Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA})
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.MULTIPART_FORM_DATA})
@GZIP
public class APIEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(APIEndpoint.class);

    private final CSVService csvService = new CSVService();
    private final SQLQueryService sqlQueryService = new SQLQueryService();
    private final TaskExecutorService taskExecutorService = new TaskExecutorService();

    @POST
    @Path("/{schemaName}")
    public Response add(@PathParam("schemaName") String schemaName, JsonQueryRequest jsonRequest) throws Exception {
        return this.executeTask(schemaName, jsonRequest.getQuery(), TaskNames.POST_TASKS);
    }

    @POST
    @Path("/upload-csv/{schemaName}/{tableName}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response addCSVUpload(@PathParam("schemaName") String schemaName, @PathParam("tableName") String tableName, InputStream inputStream, @QueryParam("delimiter") @DefaultValue(",") String delimiter, @QueryParam("skip") @DefaultValue("4") int skip, @QueryParam("limit") @DefaultValue("0") int limit) throws Exception {
        return this.csvUpload(TaskNames.INSERT_DATA_FROM_CSV, schemaName, tableName, inputStream, delimiter, skip, limit);
    }

    @PUT
    @Path("/upload-csv/{schemaName}/{tableName}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateCSVUpload(@PathParam("schemaName") String schemaName, @PathParam("tableName") String tableName, InputStream inputStream, @QueryParam("delimiter") @DefaultValue(",") String delimiter, @QueryParam("skip") @DefaultValue("4") long skip, @QueryParam("limit") @DefaultValue("0") long limit) throws Exception {
        return this.csvUpload(TaskNames.UPDATE_DATA_FROM_CSV, schemaName, tableName, inputStream, delimiter, skip, limit);
    }

    private Response csvUpload(TaskNames taskName, String schemaName, String tableName, InputStream inputStream, String delimiter, long skip, long limit) throws Exception {
        Response.Status status = Response.Status.BAD_REQUEST;
        Object response;

        LOGGER.info(taskName.toString());

        response = csvService.load(taskName, schemaName, tableName, inputStream, delimiter, skip, limit);
        if (response != null) {
            status = Response.Status.OK;
        }

        return Response.status(status).entity(response).build();
    }

    @GET
    @Path("/{schemaName}")
    public Response get(@PathParam("schemaName") String schemaName, JsonQueryRequest jsonQueryRequest) throws Exception {
        return this.executeTask(schemaName, jsonQueryRequest.getQuery(), TaskNames.GET_TASKS);
    }

    @PUT
    @Path("/{schemaName}")
    public Response update(@PathParam("schemaName") String schemaName, JsonQueryRequest jsonRequest) throws Exception {
        return this.executeTask(schemaName, jsonRequest.getQuery(), TaskNames.PUT_TASKS);
    }

    @DELETE
    @Path("/{schemaName}")
    public Response delete(@PathParam("schemaName") String schemaName, JsonQueryRequest jsonQueryRequest) throws Exception {
        return this.executeTask(schemaName, jsonQueryRequest.getQuery(), TaskNames.DELETE_TASKS);
    }

    private Response executeTask(String schemaName, String query, EnumSet<TaskNames> taskNames) throws Exception {
        Response.Status status = Response.Status.BAD_REQUEST;
        Object response = null;

        Task task = this.sqlQueryService.getTask(schemaName, query);
        LOGGER.info(task.getTaskName().toString());

        if (taskNames.contains(task.getTaskName())) {
            if (task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME) == null) {
                task.addData(TaskDataMapKeys.SCHEMA_NAME, schemaName);
            }

            response = taskExecutorService.execute(task, true);
            if (response != null) {
                status = Response.Status.OK;

                //if (TaskManager.get().canDistribute()) {
                //    switch (task.getTaskName()) {
                //        case INSERT_DATA, SELECT_DATA, UPDATE_DATA -> {}
                //        default -> TaskManager.get().distributeAsyncTaskToOtherNodes(task);
                //    }
                //}
            }
        }

        return Response.status(status).entity(response).build();
    }

}
