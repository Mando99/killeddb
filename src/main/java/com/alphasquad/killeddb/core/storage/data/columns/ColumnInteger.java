package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.ints.IntArrayList;

import java.util.*;

public class ColumnInteger extends Column<Integer> {

    public static int DEFAULT_VALUE = 0;

    public ColumnInteger(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new IntArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseInt(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseInt(newValue));
    }

    @Override
    public Integer getParsedValue(String value) {
        return Parser.parseInt(value);
    }

    @Override
    public Integer getValueAsColumnType(Object value) {
        return (Integer) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnInteger.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> ((Number) m.get(alias)).intValue())
                .filter(v -> v != ColumnInteger.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Integer getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Integer getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).intValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Integer getMin(Object currentMin, String nodeMin) {
        Integer currentMinCast = this.getValueAsColumnType(currentMin);
        return Math.min(currentMinCast, Parser.parseInt(nodeMin));
    }

    @Override
    public Integer getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Integer getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).intValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Integer getMax(Object currentMax, String nodeMax) {
        Integer currentMaxCast = this.getValueAsColumnType(currentMax);
        return Math.max(currentMaxCast, Parser.parseInt(nodeMax));
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .reduce(0, Integer::sum);
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).intValue())
                .mapToInt(Integer::intValue)
                .sum();
    }

    @Override
    public Integer getSum(Object currentMin, String nodeMin) {
        return this.getValueAsColumnType(currentMin) + Parser.parseInt(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).intValue())
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return ((Double) currentMin + Parser.parseDouble(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (int)value == Parser.parseInt(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (int)value < Parser.parseInt(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (int)value > Parser.parseInt(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
