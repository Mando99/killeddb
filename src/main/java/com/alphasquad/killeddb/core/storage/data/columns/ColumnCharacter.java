package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.chars.CharArrayList;

import java.util.*;

public class ColumnCharacter extends Column<Character> {

    public static char DEFAULT_VALUE = Character.MIN_VALUE;

    public ColumnCharacter(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new CharArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseChar(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseChar(newValue));
    }

    @Override
    public Character getParsedValue(String value) {
        return Parser.parseChar(value);
    }

    @Override
    public Character getValueAsColumnType(Object value) {
        return (Character) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnCharacter.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (char) m.get(alias))
                .filter(v -> v != ColumnCharacter.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Character getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Character getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (char) m.get(alias))
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Character getMin(Object currentMin, String nodeMin) {
        Character currentMinCast = this.getValueAsColumnType(currentMin);
        Character parsedNodeMin = nodeMin.charAt(0);
        return currentMinCast < parsedNodeMin ? currentMinCast : parsedNodeMin;
    }


    @Override
    public Character getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Character getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (char) m.get(alias))
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Character getMax(Object currentMax, String nodeMax) {
        Character currentMaxCast = this.getValueAsColumnType(currentMax);
        Character parsedNodeMax = nodeMax.charAt(0);
        return currentMaxCast > parsedNodeMax ? currentMaxCast : parsedNodeMax;
    }

    @Override
    public Number getSum() {
        return null;
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public Double getAvg() {
        return null;
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (char)value == Parser.parseChar(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return false;
    }

}
