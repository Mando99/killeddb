package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.bytes.ByteArrayList;

import java.util.*;

public class ColumnByte extends Column<Byte> {

    public static byte DEFAULT_VALUE = 0;

    public ColumnByte(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new ByteArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseByte(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseByte(newValue));
    }

    @Override
    public Byte getParsedValue(String value) {
        return Parser.parseByte(value);
    }

    @Override
    public Byte getValueAsColumnType(Object value) {
        return (Byte) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnByte.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> ((Number) m.get(alias)).byteValue())
                .filter(v -> v != ColumnByte.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Byte getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Byte getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).byteValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Byte getMin(Object currentMin, String nodeMin) {
        Byte currentMinCast = this.getValueAsColumnType(currentMin);
        Byte parsedNodeMin = Parser.parseByte(nodeMin);
        return currentMinCast < parsedNodeMin ? currentMinCast : parsedNodeMin;
    }

    @Override
    public Byte getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Byte getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).byteValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Byte getMax(Object currentMax, String nodeMax) {
        Byte currentMaxCast = this.getValueAsColumnType(currentMax);
        Byte parsedNodeMax = Parser.parseByte(nodeMax);
        return currentMaxCast > parsedNodeMax ? currentMaxCast : parsedNodeMax;
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .mapToInt(Byte::byteValue)
                .sum();
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).byteValue())
                .mapToInt(Byte::byteValue)
                .sum();
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return (Integer) currentMin + Parser.parseByte(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToInt(Byte::byteValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).byteValue())
                .mapToInt(Byte::byteValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return ((Double) currentMin + Parser.parseDouble(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (byte)value == Parser.parseByte(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (byte)value < Parser.parseByte(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (byte)value > Parser.parseByte(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
