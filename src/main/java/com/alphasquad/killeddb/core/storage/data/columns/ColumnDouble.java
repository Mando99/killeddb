package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;

import java.util.*;

public class ColumnDouble extends Column<Double> {

    public static double DEFAULT_VALUE = 0.0;

    public ColumnDouble(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new DoubleArrayList());

    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseDouble(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseDouble(newValue));
    }

    @Override
    public Double getParsedValue(String value) {
        return Parser.parseDouble(value);
    }

    @Override
    public Double getValueAsColumnType(Object value) {
        return (Double) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnDouble.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows.parallelStream()
                .map(m -> ((Number) m.get(alias)).doubleValue())
                .filter(v -> v != ColumnDouble.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Double getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Double getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).doubleValue())
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Double getMin(Object currentMin, String nodeMin) {
        Double currentMinCast = this.getValueAsColumnType(currentMin);
        return Math.min(currentMinCast, Parser.parseDouble(nodeMin));
    }

    @Override
    public Double getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Double getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).doubleValue())
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Double getMax(Object currentMax, String nodeMax) {
        Double currentMaxCast = this.getValueAsColumnType(currentMax);
        Double parsedNodeMax = Parser.parseDouble(nodeMax);
        return currentMaxCast > parsedNodeMax ? currentMaxCast : parsedNodeMax;
    }

    @Override
    public Number getSum() {
        return this.getValues()
                .parallelStream()
                .reduce(0.0, Double::sum);
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).doubleValue())
                .mapToInt(Double::byteValue)
                .sum();
    }

    @Override
    public Double getSum(Object currentMin, String nodeMin) {
        return this.getValueAsColumnType(currentMin) + Parser.parseByte(nodeMin);
    }

    @Override
    public Double getAvg() {
        return this.getValues()
                .parallelStream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> ((Number) m.get(alias)).doubleValue())
                .mapToInt(Double::byteValue)
                .average()
                .orElse(0);
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return (this.getValueAsColumnType(currentMin) + Parser.parseDouble(nodeMin)) / 2;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && (double)value == Parser.parseDouble(compValue);
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && (double)value < Parser.parseDouble(compValue);
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && (double)value > Parser.parseDouble(compValue);
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

}
