package com.alphasquad.killeddb.core.storage;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.core.storage.data.Table;

import java.util.HashMap;
import java.util.Map;

public final class MemoryDB {

    private final static Map<String, Schema> schemas = new HashMap<>();

    // SCHEMA

    public static Schema createSchema(Schema schema) throws SchemaException {
        if (MemoryDB.schemas.get(schema.getName()) != null) {
            throw new SchemaException("The schema " + schema.getName() + " already exist.");
        }

        return (MemoryDB.schemas.put(schema.getName(), schema) == null) ? schema : null;
    }

    public static Schema getSchema(String schemaName) throws SchemaException {
        Schema schema = MemoryDB.schemas.get(schemaName);

        if (schema == null) {
            throw new SchemaException("The schema " + schemaName + " does not exist.");
        }

        return schema;
    }

    public static Map<String, Schema> getSchemas() {
        return schemas;
    }

    public static Schema renameSchema(String schemaName, String newSchemaName) throws SchemaException {
        if (MemoryDB.schemas.get(newSchemaName) != null) {
            throw new SchemaException("The schema " + newSchemaName + " already exist.");
        }

        Schema schema = MemoryDB.getSchema(schemaName);
        if (!schemaName.equals(newSchemaName)) {
            MemoryDB.schemas.remove(schemaName);
            schema.setName(newSchemaName);
            MemoryDB.schemas.put(newSchemaName, schema);
        }

        return schema; 
    }

    public static Schema dropSchema(String schemaName) throws SchemaException {
        Schema schema = MemoryDB.getSchema(schemaName);
        return MemoryDB.schemas.remove(schema.getName());
    }

    // TABLE

    public static Table createTable(String schemaName, String tableName) throws SchemaException, TableException {
        Map<String, Table> tables = MemoryDB.getTables(schemaName);
        if (tables.get(tableName) != null) {
            throw new TableException("The table " + tableName + " already exist.");
        }

        Table table = new Table(tableName);
        tables.put(tableName, table);

        return table;
    }

    private static Map<String, Table> getTables(String schemaName) throws SchemaException {
        Schema schema = MemoryDB.getSchema(schemaName);
        return schema.getTables();
    }

    public static Table getTable(String schemaName, String tableName) throws SchemaException, TableException {
        Schema schema = MemoryDB.getSchema(schemaName);
        Table table = schema.getTables().get(tableName);

        if (table == null) {
            throw new TableException("The table " + tableName + " does not exist.");
        }

        return schema.getTables().get(tableName);
    }

    public static Table renameTable(String schemaName, String tableName, String newTableName) throws SchemaException, TableException {
        Table table = MemoryDB.getTable(schemaName, tableName);

        if (MemoryDB.schemas.get(schemaName).getTables().get(newTableName) != null) {
            throw new TableException("The table " + newTableName + " already exist.");
        }

        if (!tableName.equals(newTableName)) {
            MemoryDB.schemas.get(schemaName).getTables().remove(tableName);
            table.setName(newTableName);
            MemoryDB.schemas.get(schemaName).getTables().put(newTableName, table);
        }
        
        return table;
    }

    public static Table dropTable(String schemaName, String tableName) throws SchemaException, TableException {
        MemoryDB.getTable(schemaName, tableName);
        return MemoryDB.getSchema(schemaName).getTables().remove(tableName);
    }

    // COLUMN

    public static Column<Class<?>> addColumn(String schemaName, String tableName, Column<Class<?>> column) throws SchemaException, TableException, ColumnException {
        Table table = MemoryDB.getTable(schemaName, tableName);
        Map<String, Column<Class<?>>> columns = table.getColumns();
        if (columns.containsKey(column.getName())) {
            throw new ColumnException("The column " + column.getName() + " already exist.");
        }

        table.getColumns().put(column.getName(), column);
        return column;
    }

    public static Map<String, Column<Class<?>>> getColumns(String schemaName, String tableName) throws SchemaException, TableException {
        return MemoryDB.getTable(schemaName, tableName).getColumns();
    }

    public static Column<Class<?>> getColumn(String schemaName, String tableName, String columnName) throws TableException, SchemaException, ColumnException {
        Table table = MemoryDB.getTable(schemaName, tableName);

        Column<Class<?>> column = table.getColumns().get(columnName);
        if (column == null) {
            throw new ColumnException("column " + columnName + " does not exist");
        }

        return column;
    }

    public static Column<Class<?>> changeColumn(String schemaName, String tableName, String columnName, String newColumnName, ColumnTypes newColType) throws SchemaException, TableException, ColumnException {
        Column<Class<?>> column = MemoryDB.getColumn(schemaName, tableName, columnName);
        
        if (MemoryDB.getColumns(schemaName, tableName).get(newColumnName) != null) {
            throw new ColumnException("The column " + newColumnName + " already exist.");
        }
        
        if (!columnName.equals(newColumnName)) {
            MemoryDB.getTables(schemaName).get(tableName).getColumns().remove(columnName);
            column.setName(newColumnName);
            MemoryDB.getTables(schemaName).get(tableName).getColumns().put(newColumnName, column);
        }
        
        if (!column.getType().equals(newColType)) {
            MemoryDB.dropColumn(schemaName, tableName, columnName);
            column.setType(newColType);
            column.clearValues();

            return MemoryDB.addColumn(schemaName, newColumnName, column);
        }
        
        return column;
    }

    public static Column<Class<?>> dropColumn(String schemaName, String tableName, String columnName) throws SchemaException, TableException {
        return MemoryDB.getTable(schemaName, tableName).getColumns().remove(columnName);
    }


    public static long deleteAllData(String schemaName, String tableName) throws SchemaException, TableException {
        Table table = MemoryDB.getTable(schemaName, tableName);
        long size = table.getSize();
        for (Column<Class<?>> column : table.getColumns().values()) {
            column.clearValues();
        }
        table.setSize(0);
        return size;
    }

}
