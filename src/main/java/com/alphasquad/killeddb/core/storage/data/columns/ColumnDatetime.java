package com.alphasquad.killeddb.core.storage.data.columns;

import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.utils.Parser;
import it.unimi.dsi.fastutil.longs.LongArrayList;

import java.util.*;

public class ColumnDatetime extends Column<Long> {

    public static long DEFAULT_VALUE = 0;

    public ColumnDatetime(String name, ColumnTypes type) {
        super(name, type);
        this.setValues(new LongArrayList());
    }

    @Override
    public void addValue(String value) {
        this.getValues().add(Parser.parseDate(value));
    }

    @Override
    public void setValue(int index, String newValue) {
        this.getValues().set(index, Parser.parseDate(newValue));
    }

    @Override
    public Long getParsedValue(String value) {
        return Parser.parseLong(value);
    }

    @Override
    public Long getValueAsColumnType(Object value) {
        return (Long) value;
    }

    @Override
    public long getCount() {
        return this.getValues()
                .parallelStream()
                .filter(v -> v != ColumnDatetime.DEFAULT_VALUE)
                .count();
    }

    @Override
    public long getCount(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (byte) m.get(alias))
                .filter(v -> v != ColumnByte.DEFAULT_VALUE)
                .count();
    }

    @Override
    public Long getMin() {
        return this.getValues()
                .parallelStream()
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMin(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (long) m.get(alias))
                .min(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMin(Object currentMin, String nodeMin) {
        Long currentMinCast = this.getValueAsColumnType(currentMin);
        return Math.min(currentMinCast, Parser.parseLong(nodeMin));
    }


    @Override
    public Long getMax() {
        return this.getValues()
                .parallelStream()
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMax(List<Map<String, Object>> rows, String alias) {
        return rows
                .parallelStream()
                .map(m -> (long) m.get(alias))
                .max(Comparator.naturalOrder())
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Long getMax(Object currentMax, String nodeMax) {
        Long currentMaxCast = this.getValueAsColumnType(currentMax);
        return Math.max(currentMaxCast, Parser.parseLong(nodeMax));
    }

    @Override
    public Number getSum() {
        return null;
    }

    @Override
    public Number getSum(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Number getSum(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public Double getAvg() {
        return null;
    }

    @Override
    public Double getAvg(List<Map<String, Object>> rows, String alias) {
        return null;
    }

    @Override
    public Double getAvg(Object currentMin, String nodeMin) {
        return null;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return false;
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return false;
    }

    /*
    @Override
    public Date getMin() {
        return this.getValues()
                .stream()
                .min(Date::compareTo)
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Date getMax(List<Map<String, Object>> rowsListMap, Map<String, String> aliasesMap) {
        return rowsListMap
                .parallelStream()
                .map(m -> Parser.parseDate((String) m.get(aliasesMap.getOrDefault(this.getName(), this.getName()))))
                .filter(Objects::nonNull)
                .max(Date::compareTo)
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public Date getSum(List<Map<String, Object>> rowsListMap, Map<String, String> aliasesMap) {
        return null;
    }

    @Override
    public Date getAvg(List<Map<String, Object>> rowsListMap, Map<String, String> aliasesMap) {
        return null;
    }

    @Override
    public boolean equalValues(Object value, String compValue) {
        return value != null && ((Date)value).compareTo(Parser.parseDate(compValue)) == 0;
    }

    @Override
    public boolean lowerValues(Object value, String compValue) {
        return value != null && ((Date)value).compareTo(Parser.parseDate(compValue)) < 0;
    }

    @Override
    public boolean higherValues(Object value, String compValue) {
        return value != null && ((Date)value).compareTo(Parser.parseDate(compValue)) > 0;
    }

    @Override
    public boolean lowerOrEqualValues(Object value, String compValue) {
        return value != null && lowerValues(value, compValue) || equalValues(value, compValue);
    }

    @Override
    public boolean higherOrEqualValues(Object value, String compValue) {
        return value != null && higherValues(value, compValue) || equalValues(value, compValue);
    }

     */

}
