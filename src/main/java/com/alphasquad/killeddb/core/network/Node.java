package com.alphasquad.killeddb.core.network;

import com.alphasquad.killeddb.providers.GsonProvider;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jboss.resteasy.plugins.interceptors.encoding.AcceptEncodingGZIPFilter;
import org.jboss.resteasy.plugins.interceptors.encoding.GZIPDecodingInterceptor;
import org.jboss.resteasy.plugins.interceptors.encoding.GZIPEncodingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.Future;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Node {

    private static final Logger logger = LoggerFactory.getLogger(Node.class);

    @EqualsAndHashCode.Include
    private final String name;
    private final String pathPrefix;
    private boolean isCurrent;

    private Client client;
    private WebTarget target;

    private MultivaluedHashMap<String, Object> headers;

    public Node(String name, String pathPrefix, boolean isCurrent) {
        this.name = name;
        this.pathPrefix = pathPrefix;
        this.isCurrent = isCurrent;
        this.client = ClientBuilder.newClient();
        this.headers = new MultivaluedHashMap<>();
        this.headers.add("Connection", "keep-alive");
        this.headers.add("Content-Encoding", "gzip");
        this.registerClasses();
    }

    public Future<Response> pingAsync() {
        return this.client.target(this.pathPrefix + "/ping").request()
                .headers(this.headers)
                .async()
                .post(null);
    }

    public void executeTaskSync(Task task) {
        try {
            Response response = this.client.target(this.pathPrefix + "execute-task")
                    .request()
                    .headers(this.headers)
                    .post(Entity.entity(task, MediaType.APPLICATION_JSON));

            response.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public Future<Response> executeTaskAsync(Task task) {
        return this.client.target(this.pathPrefix + "execute-task")
                .request()
                .headers(this.headers)
                .async()
                .post(Entity.entity(task, MediaType.APPLICATION_JSON));
    }

    public Future<Response> insertLinesAsync(String schemaName, String tableName, char delimiter, String columnsByIndex, List<String> lines) {
        return this.client.target(this.pathPrefix + "insert-lines/" + schemaName + "/" + tableName + "/" + delimiter  + "/" + columnsByIndex)
                .request()
                .headers(this.headers)
                .async()
                .post(Entity.entity(lines, MediaType.APPLICATION_JSON));
    }

    private void registerClasses() {
        this.client.register(GsonProvider.class);
        this.client.register(AcceptEncodingGZIPFilter.class);
        this.client.register(GZIPDecodingInterceptor.class);
        this.client.register(GZIPEncodingInterceptor.class);
    }

}
