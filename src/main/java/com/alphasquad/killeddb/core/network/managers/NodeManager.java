package com.alphasquad.killeddb.core.network.managers;

import com.alphasquad.killeddb.core.network.Node;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Getter
public class NodeManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeManager.class);

    private final Properties properties = new Properties();

    private String currentIP;

    private final List<Node> nodes = new CopyOnWriteArrayList<>();
    private final List<Node> activeNodes = new CopyOnWriteArrayList<>();
    private final List<Node> inActiveNodes = new CopyOnWriteArrayList<>();

    private int nextNodeIndex;

    private NodeManager() throws IOException, ExecutionException, InterruptedException {
        this.initNodes();
        this.nextNodeIndex = 1;
    }

    public static NodeManager get() {
        return NodeManager.Instance.INSTANCE;
    }

    private void initNodes() throws IOException {
        this.properties.load(NodeManager.class.getClassLoader().getResourceAsStream("application.properties"));

        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface networkInterface : Collections.list(interfaces)){
            Enumeration<?> inetAddresses = networkInterface.getInetAddresses();
            while (inetAddresses.hasMoreElements()) {
                InetAddress inetAddress = (InetAddress) inetAddresses.nextElement();
                if (!inetAddress.isLoopbackAddress() && !inetAddress.getHostAddress().contains(":"))
                    this.currentIP = inetAddress.getHostAddress();
            }
        }

        LOGGER.info("Current IP : " + this.currentIP);

        String network = "network.";
        String protocol = ((String) this.properties.get(network + "protocol")).trim();
        String networkNodes = (String) this.properties.get(network + "nodes");
        if (networkNodes != null) {
            Set<String> ipAndPortOfNodes = new LinkedHashSet<>(List.of(StringUtils.substringBetween(networkNodes, "[", "]").replaceAll(" ", "").split(",")));

            int i = 0;
            for (String ipAndPort : ipAndPortOfNodes) {
                Node node = new Node("Node-" + i, protocol + "://" + ipAndPort + "/node/", i ==0 /* StringUtils.substringBefore(ipAndPort, ":").trim().equals(this.currentIP)*/);

                if (node.isCurrent()) {
                    this.nodes.add(0, node);
                } else {
                    this.nodes.add(node);
                }

                i++;
            }

            this.pingNodes();
        }
    }

    public void pingNodes() {
        Map<Node, Future<Response>> nodeByFutureResponse = new HashMap<>();

        this.activeNodes.clear();
        this.inActiveNodes.clear();

        for (Node node : this.nodes) {
            nodeByFutureResponse.put(node, node.pingAsync());
        }

        for (Map.Entry<Node, Future<Response>> nodeByFutureResponseEntry : nodeByFutureResponse.entrySet()) {
            try (Response response = nodeByFutureResponseEntry.getValue().get()) {
                if (response.getStatusInfo() == Response.Status.OK) {
                    this.activeNodes.add(nodeByFutureResponseEntry.getKey());
                } else {
                    this.inActiveNodes.add(nodeByFutureResponseEntry.getKey());
                }
            } catch (Exception e){
                LOGGER.error(e.getMessage());
            }
        }

        LOGGER.info("Nodes=(active=" + this.activeNodes.size() + ", inactive=" + this.inActiveNodes.size() + ").");
    }

    public synchronized Node getNextNode() {
        int activeNodesSize = this.activeNodes.size();
        Node nextNode = this.activeNodes.get(this.nextNodeIndex % activeNodesSize);
        this.nextNodeIndex = (this.nextNodeIndex + 1) % activeNodesSize;
        return nextNode;
    }

    private static class Instance {
        private static final NodeManager INSTANCE;

        static {
            try {
                INSTANCE = new NodeManager();
            } catch (IOException | ExecutionException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
