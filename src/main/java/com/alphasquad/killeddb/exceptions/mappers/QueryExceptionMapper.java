package com.alphasquad.killeddb.exceptions.mappers;

import com.alphasquad.killeddb.exceptions.QueryException;

import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.core.Response;

@Provider
public class QueryExceptionMapper implements ExceptionMapper<QueryException> {

    @Override
    public Response toResponse(QueryException e) {
        return Response.status(400).entity(e.getMessage()).type("plain/text").build();
    }

}
