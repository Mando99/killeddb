package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.core.network.Task;
import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.core.storage.data.Table;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnByte;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnString;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.enums.TaskDataMapKeys;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.QueryException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

public class TaskExecutorServiceTest {

    private final TaskExecutorService taskExecutorService = new TaskExecutorService();

    private static final List<String> SCHEMA_NAMES = new ArrayList<>();
    private static final List<String> TABLE_NAMES = new ArrayList<>();
    private static final List<Table> TABLES = new ArrayList<>();
    private static int cpt = 0;

    @After
    public void increment() {
        cpt++;
    }

    @BeforeClass
    public static void setUp() throws SchemaException, TableException {
        final int NUM_TESTS = 30;

        for (int i = 0; i < NUM_TESTS; i++) {
            SCHEMA_NAMES.add("TES-Schema" + i);
            TABLE_NAMES.add("TES-Table" + i);
        }

        for (int i = 0; i < SCHEMA_NAMES.size(); i++) {
            String schemaName = SCHEMA_NAMES.get(i);
            MemoryDB.createSchema(new Schema(schemaName));
            MemoryDB.createTable(schemaName, TABLE_NAMES.get(i));

            Column columnByte = new ColumnByte("id", ColumnTypes.BYTE);
            Column columnStr = new ColumnString("name", ColumnTypes.STRING);
            columnByte.addValues(List.of((byte)1, (byte)2, (byte)3));
            columnStr.addValues(List.of("aa", "bb", "cc"));

            Table table = MemoryDB.getTable(SCHEMA_NAMES.get(i), TABLE_NAMES.get(i));
            table.setSize(3);
            TABLES.add(table);

            table.addColumn(columnByte);
            table.addColumn(columnStr);
        }

    }

    @Test
    public void testAddSchema() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task1 = new Task(TaskNames.CREATE_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt)));
        Task task2 = new Task(TaskNames.CREATE_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, "schema1"));

        assertThrows(SchemaException.class, () -> taskExecutorService.execute(task1));

        String schemaNameFromTask = (String) task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        int numSchemas = MemoryDB.getSchemas().size();

        Schema schema = (Schema) taskExecutorService.execute(task2);
        assertEquals(MemoryDB.getSchemas().size(), numSchemas + 1);

        assertEquals(schema.getName(), schemaNameFromTask);
        assertEquals(schema.getTables().size(), MemoryDB.getSchema(schemaNameFromTask).getTables().size());
    }

    @Test
    public void testUpdateSchema() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task1 = new Task(TaskNames.RENAME_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, "schema1",
                TaskDataMapKeys.NEW_SCHEMA_NAME, "schema2"));
        Task task2 = new Task(TaskNames.RENAME_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.NEW_SCHEMA_NAME, "schema2"));

        assertThrows(SchemaException.class, () -> taskExecutorService.execute(task1));

        int numSchemas = MemoryDB.getSchemas().size();
        int numTables = MemoryDB.getSchema((String) task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME)).getTables().size();

        Schema schema = (Schema) taskExecutorService.execute(task2);
        assertEquals(MemoryDB.getSchemas().size(), numSchemas);

        assertEquals(schema.getName(), task2.getDataMap().get(TaskDataMapKeys.NEW_SCHEMA_NAME));
        assertEquals(schema.getTables().size(), numTables);
    }

    @Test
    public void testDeleteSchema() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task1 = new Task(TaskNames.DROP_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, "schema1"));
        Task task2 = new Task(TaskNames.DROP_SCHEMA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt)));

        assertThrows(SchemaException.class, () -> taskExecutorService.execute(task1));

        int numSchemas = MemoryDB.getSchemas().size();
        int numTables = MemoryDB.getSchema((String) task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME)).getTables().size();

        Schema schema = (Schema) taskExecutorService.execute(task2);
        assertEquals(MemoryDB.getSchemas().size(), numSchemas - 1);


        assertEquals(schema.getName(), task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME));
        assertEquals(schema.getTables().size(), numTables);
    }

    @Test
    public void testAddTable() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task1 = new Task(TaskNames.CREATE_TABLE, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.COLUMNS, Map.of("id", "byte",
                        "name", "string")));
        Task task2 = new Task(TaskNames.CREATE_TABLE, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, "table1",
                TaskDataMapKeys.COLUMNS, Map.of("id", "byte",
                        "name", "string")));

        assertThrows(TableException.class, () -> taskExecutorService.execute(task1));

        String schemaNameFromTask = (String) task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task2.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        int numTables = MemoryDB.getSchema(schemaNameFromTask).getTables().size();

        Table table = (Table) taskExecutorService.execute(task2);
        assertEquals(MemoryDB.getSchema(schemaNameFromTask).getTables().size(), numTables + 1);

        assertEquals(table.getName(), tableNameFromTask);
        assertEquals(table.getColumns().size(), MemoryDB.getSchema(schemaNameFromTask).getTables().get(tableNameFromTask).getColumns().size());
    }

    @Test
    public void testUpdateTable() throws SchemaException, QueryException, TableException, ColumnException {
        // TODO
    }

    @Test
    public void testDeleteTable() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task1 = new Task(TaskNames.DROP_TABLE, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, "table1"));
        Task task2 = new Task(TaskNames.DROP_TABLE, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt)));

        assertThrows(TableException.class, () -> taskExecutorService.execute(task1));

        String schemaNameFromTask = (String) task2.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task2.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        int numTables = MemoryDB.getSchema(schemaNameFromTask).getTables().size();
        int numCols = MemoryDB.getSchema(schemaNameFromTask).getTables().get(tableNameFromTask).getColumns().size();

        Table table = (Table) taskExecutorService.execute(task2);
        assertEquals(MemoryDB.getSchema(schemaNameFromTask).getTables().size(), numTables - 1);

        assertEquals(table.getName(), tableNameFromTask);
        assertEquals(table.getColumns().size(), numCols);
    }

    @Test
    public void testDeleteTableDataAll() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task = new Task(TaskNames.DELETE_DATA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.DELETE_ALL_DATA, "*"));

        String schemaNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaNameFromTask, tableNameFromTask);

        long size = TABLES.get(cpt).getSize();
        System.out.println(size);
        Object resultSize = taskExecutorService.execute(task);
        System.out.println(resultSize);
        assertEquals(resultSize, size);

        for (Column<Class<?>> col : columns.values()) {
            assertEquals(col.getValues().size(), 0);
        }
    }

    @Test
    public void testDeleteTableDataWithConditions() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task = new Task(TaskNames.DELETE_DATA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.WHERE_AND_OR_CONDITIONS, List.of("id", ">=", "2")));

        String schemaNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaNameFromTask, tableNameFromTask);

        String resultMessage = (String) taskExecutorService.execute(task);
        assertEquals(resultMessage, "Rows have been successfully deleted.");

        for (Column<Class<?>> col : columns.values()) {
            assertEquals(col.getValues().size(), 1);
        }
    }

    @Test
    public void testAddColumns() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task = new Task(TaskNames.ADD_COLUMNS, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.COLUMNS_AND_TYPES, Map.of(
                        "id1", "string",
                        "name1", "byte",
                        "new", "byte")
        ));

        String schemaNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaNameFromTask, tableNameFromTask);
        int numCols = columns.size();
        taskExecutorService.execute(task);
        assertEquals(MemoryDB.getTable(schemaNameFromTask, tableNameFromTask).getColumns().size(), numCols + 3);
    }

    @Test
    public void testUpdateColumns() throws SchemaException, QueryException, TableException, ColumnException {
        // TODO
    }

    @Test
    public void testDeleteColumns() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task = new Task(TaskNames.DROP_COLUMNS, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.COLUMNS, new String[]{"x", "id", "y"}));

        String schemaNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaNameFromTask, tableNameFromTask);
        int numCols = columns.size();

        assertEquals(taskExecutorService.execute(task), task.getDataMap().get(TaskDataMapKeys.COLUMNS));
        assertEquals(MemoryDB.getTable(schemaNameFromTask, tableNameFromTask).getColumns().size(), numCols - 1);
    }

    @Test
    public void testAddRows() throws SchemaException, QueryException, TableException, ColumnException, ExecutionException, InterruptedException {
        Task task = new Task(TaskNames.INSERT_DATA, Map.of(TaskDataMapKeys.SCHEMA_NAME, SCHEMA_NAMES.get(cpt),
                TaskDataMapKeys.TABLE_NAME, TABLE_NAMES.get(cpt),
                TaskDataMapKeys.COLUMNS, List.of("x", "id", "y"),
                TaskDataMapKeys.COLUMNS_VALUES, List.of(
                        Map.of(
                                "x", "aa",
                                "id", "1",
                                "y", "aa"
                        ),
                        Map.of("id", "2")
                )));


        String schemaNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.SCHEMA_NAME);
        String tableNameFromTask = (String) task.getDataMap().get(TaskDataMapKeys.TABLE_NAME);
        Map<String, Column<Class<?>>> columns = MemoryDB.getColumns(schemaNameFromTask, tableNameFromTask);
        int numValues = columns.get("id").getValues().size();

        assertEquals(taskExecutorService.execute(task), TABLES.get(cpt).getColumns());

        for (Column<Class<?>> col : columns.values()) {
            assertEquals(col.getValues().size(), numValues + 2);
        }
    }


}
