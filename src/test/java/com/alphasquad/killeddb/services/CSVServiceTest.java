package com.alphasquad.killeddb.services;

import com.alphasquad.killeddb.core.storage.MemoryDB;
import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnByte;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnDatetime;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.enums.TaskNames;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class CSVServiceTest {
    private final CSVService csvService = new CSVService();
    private InputStream inputStream = null;

    @Before
    public void setUp() throws IOException, SchemaException, TableException, ColumnException {
        File file = new File("./src/test/data/data.csv");
        this.inputStream = new FileInputStream(file);

        MemoryDB.createSchema(new Schema("CSV-Schema1"));
        MemoryDB.createTable("CSV-Schema1", "CSV-Table1");

        // (vendorID byte, tpep_pickup_datetime datetime, tpep_dropoff_datetime datetime, passenger_count byte, trip_distance float, pickup_longitude float, pickup_latitude float, RatecodeID byte, store_and_fwd_flag char, dropoff_longitude float, dropoff_latitude float, payment_type byte, fare_amount float, extra float, mta_tax float, tip_amount float, tolls_amount float, improvement_surcharge float, total_amount float)"
        Column col1 = new ColumnByte("vendorID", ColumnTypes.BYTE);
        Column col2 = new ColumnDatetime("tpep_pickup_datetime", ColumnTypes.DATETIME);
        Column col3 = new ColumnDatetime("tpep_dropoff_datetime", ColumnTypes.DATETIME);
        Column col4 = new ColumnByte("passenger_count", ColumnTypes.BYTE);

        MemoryDB.addColumn("CSV-Schema1", "CSV-Table1", col1);
        MemoryDB.addColumn("CSV-Schema1", "CSV-Table1", col2);
        MemoryDB.addColumn("CSV-Schema1", "CSV-Table1", col3);
        MemoryDB.addColumn("CSV-Schema1", "CSV-Table1", col4);
    }

    //@Test
    //public void testLoad() throws Exception {
    //    csvService.load(TaskNames.INSERT_DATA_FROM_CSV, "CSV-Schema1", "CSV-Table1", inputStream, ",", 0, 1000);
    //    // assertEquals(MemoryDB.getTable("CSV-Schema1", "CSV-Table1").getSize(), 4);
    //}
}
