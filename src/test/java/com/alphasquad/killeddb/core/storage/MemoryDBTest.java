package com.alphasquad.killeddb.core.storage;

import com.alphasquad.killeddb.core.storage.data.Schema;
import com.alphasquad.killeddb.core.storage.data.Table;
import com.alphasquad.killeddb.core.storage.data.columns.Column;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnByte;
import com.alphasquad.killeddb.core.storage.data.columns.ColumnString;
import com.alphasquad.killeddb.enums.ColumnTypes;
import com.alphasquad.killeddb.exceptions.ColumnException;
import com.alphasquad.killeddb.exceptions.SchemaException;
import com.alphasquad.killeddb.exceptions.TableException;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class MemoryDBTest {

    private static final List<String> SCHEMA_NAMES = new ArrayList<>();
    private static final List<String> TABLE_NAMES = new ArrayList<>();
    private static final List<Table> TABLES = new ArrayList<>();
    private static int cpt = 0;

    @After
    public void increment() {
        cpt++;
    }

    @BeforeClass
    public static void setUp() throws SchemaException, TableException, ColumnException {
        final int NUM_TESTS = 30;

        for (int i = 0; i < NUM_TESTS; i++) {
            SCHEMA_NAMES.add("MDB-Schema" + i);
            TABLE_NAMES.add("MDB-Table" + i);
        }

        for (int i = 0; i < SCHEMA_NAMES.size(); i++) {
            String schemaName = SCHEMA_NAMES.get(i);
            MemoryDB.createSchema(new Schema(schemaName));
            MemoryDB.createTable(schemaName, TABLE_NAMES.get(i));

            Column columnByte = new ColumnByte("id", ColumnTypes.BYTE);
            Column columnStr = new ColumnString("name", ColumnTypes.STRING);
            columnByte.addValues(List.of((byte)1, (byte)2, (byte)3));
            columnStr.addValues(List.of("aa", "bb", "cc"));

            Table table = MemoryDB.getTable(SCHEMA_NAMES.get(i), TABLE_NAMES.get(i));
            table.setSize(3);
            TABLES.add(table);

            table.addColumn(columnByte);
            table.addColumn(columnStr);
        }

    }

    @Test
    public void testCreateSchema() throws SchemaException {
        MemoryDB.createSchema(new Schema("Schema1000"));
        MemoryDB.createSchema(new Schema("Schema1001"));
        MemoryDB.createSchema(new Schema("Schema1002"));

        assertThrows(SchemaException.class, () -> MemoryDB.createSchema(new Schema(SCHEMA_NAMES.get(0))));
        assertEquals(MemoryDB.getSchemas().size(), 33);
    }

    @Test
    public void testCreateTable() throws SchemaException, TableException {
        assertThrows(TableException.class, () -> MemoryDB.createTable(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt)));

        Table table = MemoryDB.createTable(SCHEMA_NAMES.get(cpt), "newTable");

        assertEquals(MemoryDB.getSchema(SCHEMA_NAMES.get(cpt)).getTables().size(), 2);
    }

    @Test
    public void testGetSchema() throws SchemaException {
        assertThrows(SchemaException.class, () -> MemoryDB.getSchema("xxxx"));

        Schema schema = MemoryDB.getSchema(SCHEMA_NAMES.get(cpt));
        assertEquals(schema.getName(), SCHEMA_NAMES.get(cpt));
        assertEquals(schema.getTables().size(), 1);
    }

    @Test
    public void testGetTable() throws SchemaException, TableException {
        assertThrows(TableException.class, () -> MemoryDB.getTable(SCHEMA_NAMES.get(cpt), "xxxx"));

        Table table = MemoryDB.getTable(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt));

        assertEquals(table.getName(), TABLE_NAMES.get(cpt));
        assertEquals(TABLES.get(cpt).getName(), table.getName());
        assertEquals(TABLES.get(cpt).getColumns().size(), table.getColumns().size());
    }

    @Test
    public void testGetColumn() throws SchemaException, TableException, ColumnException {
        assertThrows(ColumnException.class, () -> MemoryDB.getColumn(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt), "xxxx"));

        Column column = MemoryDB.getColumn(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt), "id");
        assertEquals(TABLES.get(cpt).getColumns().get("id").getName(), column.getName());
        assertEquals(TABLES.get(cpt).getColumns().get("id").getValues().size(), column.getValues().size());
    }

    @Test
    public void testRenameSchema() throws SchemaException {
        assertThrows(SchemaException.class, () -> MemoryDB.renameSchema("xxx", "xxx"));
        assertThrows(SchemaException.class, () -> MemoryDB.renameSchema(SCHEMA_NAMES.get(cpt), SCHEMA_NAMES.get(cpt)));

        Schema newSchema = MemoryDB.renameSchema(SCHEMA_NAMES.get(cpt), "xxx");
        assertThrows(SchemaException.class, () -> MemoryDB.getSchema(SCHEMA_NAMES.get(cpt)));
        assertEquals(newSchema.getName(), "xxx");
        assertEquals(newSchema.getTables().size(), 1);
    }

    @Test
    public void testAddColumn() throws SchemaException, TableException, ColumnException {
        Column columnByte = new ColumnByte("name", ColumnTypes.BYTE);
        Column columnStr = new ColumnString("name", ColumnTypes.STRING);
        assertThrows(SchemaException.class, () -> MemoryDB.addColumn("xxxx", TABLE_NAMES.get(cpt), columnByte));
        assertThrows(TableException.class, () -> MemoryDB.addColumn(SCHEMA_NAMES.get(cpt), "xxx", columnByte));

        assertThrows(ColumnException.class, () -> MemoryDB.addColumn(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt), columnByte));
        assertThrows(ColumnException.class, () -> MemoryDB.addColumn(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt), columnStr));

        columnStr.setName("name2");
        int numColumns = MemoryDB.getTable(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt)).getColumns().size();
        MemoryDB.addColumn(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt), columnStr);
        assertEquals(MemoryDB.getTable(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt)).getColumns().size(), numColumns + 1);
    }

    @Test
    public void testDeleteAllData() throws SchemaException, TableException {
        assertEquals(MemoryDB.deleteAllData(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt)), 3);
        for (Column col : MemoryDB.getColumns(SCHEMA_NAMES.get(cpt), TABLE_NAMES.get(cpt)).values()) {
            assertEquals(col.getValues().size(), 0);
        }
    }

}
