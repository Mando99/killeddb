package com.alphasquad.killeddb.utils;

import com.google.common.collect.Comparators;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class SorterTest {

    public static boolean isSortedStr(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.naturalOrder());
    }

    public static boolean isSortedInt(List<Integer> listOfNums) {
        return Comparators.isInOrder(listOfNums, Comparator.naturalOrder());
    }


    @Test
    public void testSortNumberList() {
        List<Map<String, Object>> list = new ArrayList<>(List.of(Map.of("id", 2), Map.of( "id", 1), Map.of("id", 3)));
        Sorter.sortNumberList(list, "id", "desc", Map.of("id", "ID"));

        List<Integer> sortedList = new ArrayList<>();
        for (Map<String, Object> m : list) {
            sortedList.add((int) m.get("id"));
        }
        Collections.reverse(sortedList);
        assertTrue(isSortedInt(sortedList));

        Sorter.sortNumberList(list, "id", "asc", Map.of("id", "ID"));
        sortedList = new ArrayList<>();
        for (Map<String, Object> m : list) {
            sortedList.add((int) m.get("id"));
        }
        assertTrue(isSortedInt(sortedList));
    }

    @Test
    public void testSortStringList() {
        List<Map<String, Object>> list = new ArrayList<>(List.of(Map.of("id", "abc"), Map.of( "id", "abb"), Map.of("id", "aba")));
        Sorter.sortStringList(list, "id", "desc", Map.of("id", "ID"));

        List<String> sortedList = new ArrayList<>();
        for (Map<String, Object> m : list) {
            sortedList.add((String) m.get("id"));
        }
        Collections.reverse(sortedList);
        assertTrue(isSortedStr(sortedList));

        Sorter.sortStringList(list, "id", "asc", Map.of("id", "ID"));
        sortedList = new ArrayList<>();
        for (Map<String, Object> m : list) {
            sortedList.add((String) m.get("id"));
        }
        assertTrue(isSortedStr(sortedList));
    }
}
